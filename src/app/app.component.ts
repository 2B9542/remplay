import { Component, OnInit, ViewChild, ElementRef, } from '@angular/core';
import { ElectronService } from "ngx-electron";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  title = 'remplay';
  desktopCapturer: Electron.DesktopCapturer;
  Menu: typeof Electron.Menu;
  stream: MediaStream;
  //@ts-ignore
  recorder: MediaRecorder;

  recordingInterval: NodeJS.Timeout;
  
  server: HTMLVideoElement;
  @ViewChild("server")
  set serverVideo(value: ElementRef) {
    this.server = value.nativeElement;    
  }

  client: HTMLVideoElement;
  @ViewChild("client")
  set clientVideo(value: ElementRef) {
    this.client = value.nativeElement;    
  }

  constructor(private electron: ElectronService) {
  } 
  
  ngOnInit() {
    this.desktopCapturer = this.electron.desktopCapturer;
    this.Menu = this.electron.remote.Menu;
  }
  
  async getSources() {
    const sources = await this.desktopCapturer.getSources({
      types: [
        "window", "screen",
      ]
    });

    const menu = this.Menu.buildFromTemplate(
      sources.map(s => ({
        label: s.name,
        click: () => this.selectSource(s),
      }))
    );

    menu.popup();
  }

  async selectSource(source) {
    const constraints: MediaStreamConstraints = {
      audio: false,
      video: {
        //@ts-ignore
        mandatory: {
          chromeMediaSource: "desktop",
          chromeMediaSourceId: source.id,
          minWidth: 1920,
          maxWidth: 1920,
          minHeight: 1080,
          maxHeight: 1080,
        },
      }
    };

    this.stream = await navigator.mediaDevices.getUserMedia(constraints);

    const options = {
      mimeType: "video/webm; codecs=vp9"
    };
    //@ts-ignore
    this.recorder = new MediaRecorder(this.stream, options);
    this.recorder.ondataavailable = async (e) => {
      const buffer = Buffer.from(await e.data.arrayBuffer());
      console.log(buffer);

      this.client.srcObject = e.data;
    };
    this.recorder.onstop = () => {
      console.log("stopped")
    };
  }

  async start() {
    if(this.server === undefined || this.server === null)
      return;

    if(!this.server.paused)
      return;

    if(this.server.srcObject === null || this.server.srcObject === undefined)
      return;

    this.server.play();
    this.server.play();

    this.startRecording();
  }

  startRecording() {
    this.recorder.start();
    const fps = 60;
    const delta = (1000 / fps);
    this.recordingInterval = setInterval(async () => {
      this.recorder.requestData();
    }, delta);
  }

  stopRecording() {
    clearInterval(this.recordingInterval);
  }

  async stop() {
    if(this.server === undefined || this.server === null)
      return;

    if(this.server.paused)
      return;

    if(this.server.srcObject === null || this.server.srcObject === undefined)
      return;

    this.server.pause();
    this.recorder.stop();

    this.stopRecording();
  }

  public get startButtonClasses() {
    return {
      disabled: (this.server !== undefined && this.server !== null) && ((!this.server.paused) || (this.server.srcObject === null || this.server.srcObject === undefined)),
    };
  }

  public get stopButtonClasses() {
    return {
      disabled: (this.server !== undefined && this.server !== null) && ((this.server.paused) || (this.server.srcObject === null || this.server.srcObject === undefined)),
    };
  }
}

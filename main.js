const { app, BrowserWindow, } = require("electron");
const url = require("url");

let win;

function createWindow() {
	win = new BrowserWindow({
		// width: 1280,
		// height: 720,
		backgroundColor: "#333",
		webPreferences: {
			nodeIntegration: true,
			enableRemoteModule: true,
		},
		fullscreen: true,
	});
	// win.webContents.openDevTools();

	win.loadURL(url.format(`file://${__dirname}/dist/remplay/index.html`));

	win.on("closed", () => {
		win = null;
	});
}

app.on("ready", createWindow);
app.on("window-all-closed", () => {
	if(process.platform !== "darwin")
		app.quit();
});

app.on("activate", () => {
	if(win === null)
		createWindow();
});
